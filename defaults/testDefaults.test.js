const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const customDefault = require("./defaults")
let defaultProps = { name: 'Utkarsh', age: "55", superPowers: 'billionare' }
let myRes = customDefault(testObject, defaultProps)
let expectedRes = JSON.stringify({
    name: 'Bruce Wayne',
    age: 36,
    superPowers: 'billionare',
    location: 'Gotham'
}
)
console.log(expectedRes)
console.log(myRes)

test('should return object in which every undefined field is filed by defaultProps', () => {
    expect(myRes).toEqual(expectedRes)
})