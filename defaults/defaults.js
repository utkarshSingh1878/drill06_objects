// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
let defaultProps = { name: 'Utkarsh', age: "55", superPowers: 'billionare' }

function defaults(obj, defaultProps) {
    return JSON.stringify({ ...defaultProps, ...obj })
}
// console.log(
//     defaults(testObject, defaultProps)
// )

module.exports = defaults