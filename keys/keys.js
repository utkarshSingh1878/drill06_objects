// use this object to test your functions
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys
function keys(obj) {
    let allKeys = []
    for (key in obj) {
        allKeys.push(String(key))
    }
    return allKeys
}
module.exports=keys
