// use this object to test your functions
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const customKeys = require("./keys")

const expectedRes = Object.keys(testObject)
const myRes = customKeys(testObject)

test('should return all the keys from object', () => {
    expect(expectedRes).toEqual(myRes)
})