
// use this object to test your functions
const testObject = {
    name: 'Bruce Wayne', age: 36, location: 'Gotham'
};

// Convert an object into a list of [key, value] pairs.
//  http://underscorejs.org/#pairs
function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    let keyValueList = []
    for (key in obj) {
        keyValueList.push([key, obj[key]]);
    }
    return keyValueList
}
module.exports = pairs;
