const testObject = {
    name: 'Bruce Wayne', age: 36, location: 'Gotham'
};
// console.log(
//     Object.entries(testObject)
// )

const customPairs = require("./pairs")
const expectedRes = Object.entries(testObject)
const myRes = customPairs(testObject)

test('should return [key,value] pairs', () => {
    expect(expectedRes).toEqual(myRes)
})