
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
function mapObject(obj, cb) {
    const res = cb(obj)
    return res
}
function cb(obj) {
    for (key in obj) {
        obj[key] = `${obj[key]} is updated`
    }
    return obj
}
module.exports = mapObject