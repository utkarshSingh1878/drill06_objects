const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const customMapObject = require("./mapObject")
function cb(obj) {
    for (key in obj) {
        obj[key] = `${obj[key]} is updated`
    }
    return obj
}
let myRes = customMapObject(testObject, cb)
let expectedRes = {
    name: 'Bruce Wayne is updated',
    age: '36 is updated',
    location: 'Gotham is updated'
}

test('should update values', () => {
    expect(expectedRes).toEqual(myRes)
})