const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const customInvert = require("./invert")

let myRes = customInvert(testObject)
let expectedRes = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }

test('should return an inverted object i.e - key:value => value:key', () => {
    expect(myRes).toEqual(expectedRes)
})